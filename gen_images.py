#!/usr/bin/env python2
from PyQt4.Qt import *
import sys
a = QApplication(sys.argv)
f = QFont("Avenir Next LT Pro", 144)
fm = QFontMetrics(f)
color = QColor.fromRgb(255, 1, 3)
br = QBrush(color)
pen = QPen(color)
whitebrush = QBrush(QColor(Qt.white))
whitepen = QPen(QColor(Qt.white), 12, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
margin = QSize(100, 100)
maxw = 512
for l in sys.stdin:
    l = l.strip().decode('utf-8')
    sz0 = fm.size(0, l)
    sz = margin + sz0
    img = QImage(sz, QImage.Format_ARGB32_Premultiplied)
    img.fill(QColor.fromRgb(0,0,0,0))
    p = QPainter(img)
    p.setRenderHint(QPainter.Antialiasing)
    p.setFont(f)
    pos = QPoint(margin.width()/2, margin.height()/2*1.2 + fm.ascent())
    pp = QPainterPath()
    pp.addText(pos, f, l)
    p.strokePath(pp, whitepen)
    p.fillPath(pp, whitebrush)
    p.setBrush(br)
    p.setPen(pen)
    p.drawText(pos, l)
    del p
    img.scaledToWidth(maxw, Qt.SmoothTransformation).save(l+".png")
    del img
